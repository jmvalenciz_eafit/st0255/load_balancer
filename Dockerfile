FROM nginx:alpine

COPY config/nginx.conf /etc/nginx

EXPOSE 8080
